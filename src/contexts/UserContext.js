import React, { useContext, useState, useEffect } from 'react'
import { auth } from '../firebase'
import { useDatabase } from './DatabaseContext'

const UserContext = React.createContext()

export function useUser() {
  return useContext(UserContext)
}

export function UserProvider({ children }) {
  const [currentUser, setCurrentUser] = useState({})
  const [userActive, setUserActive] = useState(false)
  const { get, subscribe } = useDatabase()
  
  useEffect(() => {
    const updateUser =  (user) => {
      setCurrentUser(user.data())
    }
    auth.onAuthStateChanged(async (user) => {
      if ((!user && !userActive) || (user && userActive)) return
      if (user && !userActive) {
        const record = await get({ collection: 'users', id: user.uid })
        setCurrentUser(record.data())
        setUserActive(true)
        const unsubscribe = subscribe({ collection: 'users', id: user.uid, callback: updateUser })
        return unsubscribe
      }
      if (!user && userActive) {
        setCurrentUser({})
        setUserActive(false)
      }
    })
  }, [currentUser, userActive, subscribe, get])
  
  const value = {
    tokens: currentUser.tokens
  }

  return (
    <UserContext.Provider value={value}>
      {children}
    </UserContext.Provider>
  )
}
