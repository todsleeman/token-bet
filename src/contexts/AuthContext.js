import React, { useContext, useState, useEffect } from 'react'
import { auth } from '../firebase'

const AuthContext = React.createContext()

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  const [currentAuthUser, setCurrentAuthUser] = useState()
  const [loading, setLoading] = useState(true)
  
  function signup(email, password) {
    return auth.createUserWithEmailAndPassword(email, password)
  }
  
  function login(email, password) {
    return auth.signInWithEmailAndPassword(email, password)
  }
  
  function signOut() {
    return auth.signOut()
  }
  
  function resetPassword(email) {
    return auth.sendPasswordResetEmail(email)
  }
  
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(user => {
      setCurrentAuthUser(user)
      setLoading(false)
    })
    return unsubscribe
  }, [])
  
  const value = {
    currentAuthUser,
    signup,
    login,
    signOut,
    resetPassword
  }

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
