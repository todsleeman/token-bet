import React, { useContext } from 'react'
import { db } from '../firebase'

const DatabaseContext = React.createContext()

export function useDatabase() {
  return useContext(DatabaseContext)
}

export function DatabaseProvider({ children }) {
  function store({ collection, id, data, merge = true }) {
    return db.collection(collection).doc(id).set(data, { merge })
  }

  function get({ collection, id }) {
    return db.collection(collection).doc(id).get()
  }

  function subscribe({ collection, id, callback }) {
    if (id) return db.collection(collection).doc(id).onSnapshot(callback)
    return db.collection(collection).onSnapshot(callback)
  }
  
  const value = {
    store,
    get,
    subscribe
  }

  return (
    <DatabaseContext.Provider value={value}>
      {children}
    </DatabaseContext.Provider>
  )
}
