import React, { useRef, useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Card, Form, Button, Alert } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext'
import lottie from 'lottie-web'

export default function Login() {
  const email = useRef()
  const password = useRef()
  const container = useRef()
  const { login } = useAuth()
  const [error, setError] = useState('')
  const [loading , setLoading] = useState(false)
  const history = useHistory()

  useEffect(() => {
    lottie.loadAnimation({
      container: container.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: require('../animations/coin-jump.json')
    })
  }, [])

  async function handleSubmit(e) {
    e.preventDefault()
    try {
      setError('')
      setLoading(true)
      await login(email.current.value, password.current.value)
      history.push('/')
    } catch (e) {
      setLoading(false)
      return setError(`Failed to log in: ${e.message} `)
    }
    setLoading(false)
  }

  return (
    <>
      <div className="mb-3" ref={container}/>
      <Card>
        <Card.Body>
          <h3 className="w-100 text-center mb-4">Log In</h3>
          {error && <Alert variant="danger">{error}</Alert> }
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" ref={email} required />
            </Form.Group>
            <Form.Group id="password">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" ref={password} required />
            </Form.Group>
            <Button disabled={loading} className="w-100" type="submit">
              Log In
            </Button>
            <div className="w-100 text-center mt-3">
              <h6><Link to="/forgot-password">Forgot Password?</Link></h6>
            </div>
          </Form>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <h6>Need an account? <Link to="/signup">Sign Up</Link></h6>
      </div>
    </>
  )
}
