import React, { useRef, useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Card, Form, Button, Alert } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext'
import { useDatabase } from '../contexts/DatabaseContext'
import lottie from 'lottie-web'

export default function SignUp() {
  const username = useRef()
  const email = useRef()
  const password = useRef()
  const passwordConfirm = useRef()
  const container = useRef()
  const { signup } = useAuth()
  const { store } = useDatabase()
  const [error, setError] = useState('')
  const [loading , setLoading] = useState(false)
  const history = useHistory()

  useEffect(() => {
    lottie.loadAnimation({
      container: container.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: require('../animations/super-coin.json')
    })
  }, [])

  async function handleSubmit(e) {
    e.preventDefault()
    if (password.current.value !== passwordConfirm.current.value) {
      return setError('Passwords do not match')
    }
    try {
      setError('')
      setLoading(true)
      const { user } = await signup(email.current.value, password.current.value)
      const record = {
        id: user.uid,
        email: user.email,
        tokens: 1000
      }
      await store({ collection: 'users', id: record.id, data: record })
      history.push('/')
    } catch (error) {
      return setError(`Failed to create an account: ${error.message}`)
    }
    setLoading(false)
  }

  return (
    <>
      <div className="mb-3" ref={container}/>
      <Card>
        <Card.Body>
          <h3 className="w-100 text-center mb-4">Sign Up</h3>
          {error && <Alert variant="danger">{error}</Alert> }
          <Form onSubmit={handleSubmit}>
            <Form.Group id="username">
              <Form.Label>Username</Form.Label>
              <Form.Control type="text" ref={username} required />
            </Form.Group>
            <Form.Group id="email">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" ref={email} required />
            </Form.Group>
            <Form.Group id="password">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" ref={password} required />
            </Form.Group>
            <Form.Group id="password-confirm">
              <Form.Label>Password Confirmation</Form.Label>
              <Form.Control type="password" ref={passwordConfirm} required />
            </Form.Group>
            <Button disabled={loading} className="w-100" type="submit">
              Sign Up
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <h6>Already have an account? <Link to="/login">Log In</Link></h6>
      </div>
    </>
  )
}
