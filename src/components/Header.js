import React, { useState } from 'react'
import { useAuth } from '../contexts/AuthContext'
import { Button, Alert } from 'react-bootstrap'
import logo from '../images/logo.png'

export default function Header() {
  const [error, setError] = useState('')
  const { currentAuthUser, signOut } = useAuth()

  async function logout (e) {
    e.preventDefault()
    try {
      setError('')
      await signOut()
    } catch (e) {
      setError(e.message)
    }
  }

  return (
    <div className="header w-100">
      <div className="logo-image">
        <img className="logo" src={logo} alt="logo"></img>
      </div>
      <div className="edit-profile float-right">
        {error && <Alert variant="danger">{error}</Alert> }
        {currentAuthUser && <Button variant="warning" className="mr-2">Edit Profile</Button> }
        {currentAuthUser && <Button onClick={logout} variant="danger">Log Out</Button> }
      </div>
    </div>
  )
}
