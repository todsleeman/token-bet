import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'

export function PrivateRoute({ component: Component, ...rest}) {
  const { currentAuthUser } = useAuth()

  return (
    <Route
      {...rest}
      render={props => {
        return currentAuthUser ? <Component {...props} /> : <Redirect to="/login" />
      }}
    />
  )
}

export function PublicRoute({ component: Component, ...rest}) {
  const { currentAuthUser } = useAuth()

  return (
    <Route
      {...rest}
      render={props => {
        return currentAuthUser ? <Redirect to="/" /> : <Component {...props} />
      }}
    />
  )
}