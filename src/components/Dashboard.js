import React, { useRef, useEffect } from 'react'
import { Button } from 'react-bootstrap'
import lottie from 'lottie-web'
import { useUser } from '../contexts/UserContext'
import AnimatedNumber from 'animated-number-react'
import token from '../images/coin.svg'

export default function Dashboard() {
  const container = useRef()
  const { tokens } = useUser()
  const formatValue = (tokens) => tokens.toFixed(0);

  useEffect(() => {
    lottie.loadAnimation({
      container: container.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: require('../animations/super-coin-flag.json')
    })
  }, [])

  return (
    <>
      <div className="mb-3" ref={container}/>
      {tokens && 
        <div className="d-flex justify-content-center align-items-center mb-3">
          <img src={token} alt="token" className="coin"/>
          <span className="text-white coin-text">
            <h5>
              <AnimatedNumber
                value={tokens}
                formatValue={formatValue}
              />
            </h5>
          </span>
        </div>
      }
      <Button className="w-100 mb-2" variant="success">
          Make a Bet
      </Button>
      <Button className="w-100 mb-2" variant="info">
          Send a Gift
      </Button>
      <Button className="w-100 mb-2">
          Add Friends
      </Button>
    </>
  )
}
