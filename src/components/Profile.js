import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'
import { Button, Alert } from 'react-bootstrap'

export default function Profile() {
  const [error, setError] = useState('')
  const history = useHistory()
  const { signOut } = useAuth()

  async function logout (e) {
    e.preventDefault()
    try {
      setError('')
      await signOut()
      history.push('/login')
    } catch (e) {
      setError(e.message)
    }
  }

  return (
    <div>
      <Button onClick={logout} className="w-100 mb-2" variant="danger">
          Log Out
      </Button>
      {error && <Alert variant="danger">{error}</Alert> }
    </div>
  )
}
