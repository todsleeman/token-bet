import React, { useRef, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Card, Form, Button, Alert } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext'
import lottie from 'lottie-web'

export default function ForgotPassword() {
  const email = useRef()
  const container = useRef()
  const { resetPassword } = useAuth()
  const [error, setError] = useState('')
  const [message, setMessage] = useState('')
  const [loading , setLoading] = useState(false)

  useEffect(() => {
    lottie.loadAnimation({
      container: container.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: require('../animations/coin-speech.json')
    })
  }, [])

  async function handleSubmit(e) {
    e.preventDefault()
    try {
      setError('')
      setMessage('')
      setLoading(true)
      await resetPassword(email.current.value)
      setMessage('Please check your inbox for further instructions')
    } catch (e) {
      setLoading(false)
      return setError(`Failed to reset password: ${e.message} `)
    }
    setLoading(false)
  }

  return (
    <>
      <div className="mb-3" ref={container}/>
      <Card>
        <Card.Body>
          <h3 className="w-100 text-center mb-4">Password Reset</h3>
          {message && <Alert variant="success">{message}</Alert> }
          {error && <Alert variant="danger">{error}</Alert> }
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" ref={email} required />
            </Form.Group>
            <Button disabled={loading} className="w-100" type="submit">
              Reset Password
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <h6><Link to="/login">Log In</Link></h6>
      </div>
    </>
  )
}
