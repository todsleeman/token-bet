import React from 'react'
import './App.css';
import Header from './components/Header'
import Dashboard from './components/Dashboard'
import SignUp from './components/SignUp'
import Login from './components/Login'
import ForgotPassword from './components/ForgotPassword'
import { PrivateRoute, PublicRoute } from './components/Routing'
import { Container } from 'react-bootstrap' 
import { AuthProvider } from './contexts/AuthContext'
import { DatabaseProvider } from './contexts/DatabaseContext'
import { UserProvider } from './contexts/UserContext'
import { BrowserRouter as Router, Switch } from 'react-router-dom'

export default function App () {
  return (
    <div className="App">
      <DatabaseProvider>
        <AuthProvider>
          <UserProvider>
            <Header></Header>
            <Container
              className="w-100 d-flex align-items-center justify-content-center"
              style={{ minHeight: "100vh"}}
            >
              <div className="w-100" style={{ maxWidth: "400px" }}>
                <Router>
                  <Switch>
                    <PrivateRoute exact path="/" component={Dashboard} />
                    <PublicRoute path="/signup" component={SignUp} />
                    <PublicRoute path="/login" component={Login} />
                    <PublicRoute path="/forgot-password" component={ForgotPassword} />
                  </Switch>
                </Router>
              </div>
            </Container>
          </UserProvider>
        </AuthProvider>
      </DatabaseProvider>
    </div>
  )
}
